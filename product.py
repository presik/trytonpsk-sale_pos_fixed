# This file is part of sale_pos module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import fields
from trytond.pool import PoolMeta, Pool


__all__ = ['Template']


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'
    sale_price_w_tax = fields.Numeric('Sale Price With Tax', digits=(16, 2),
        depends=['list_price', 'customer_taxes'], required=True)

    # @fields.depends('customer_taxes', 'sale_price_w_tax', 'customer_taxes_used')
    # def on_change_with_list_price(self):
    #     if self.sale_price_w_tax:
    #         res = self.compute_reverse_list_price(self.sale_price_w_tax)
    #         return res

    def compute_reverse_list_price(self, price_w_tax):
        Tax = Pool().get('account.tax')
        res = Tax.reverse_compute(price_w_tax, self.customer_taxes_used)
        res = res.quantize(
            Decimal(1) / 10 ** self.__class__.list_price.digits[1])
        return res
