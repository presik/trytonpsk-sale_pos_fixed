# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import Pool
from . import sale
from . import shop
from . import statement
from . import product
from . import device
from . import user
from . import invoice


def register():
    Pool.register(
        invoice.Invoice,
        product.Template,
        device.SaleDevice,
        user.User,
        statement.Journal,
        sale.Sale,
        sale.SaleLine,
        statement.Statement,
        statement.StatementLine,
        sale.AddProductForm,
        sale.SalePaymentForm,
        shop.SaleShop,
        shop.ShopDailySummaryStart,
        sale.SaleUpdateDateStart,
        sale.SaleDetailedStart,
        sale.SaleIncomeDailyStart,
        device.SaleDeviceStatementJournal,
        statement.OpenStatementStart,
        statement.OpenStatementDone,
        statement.CloseStatementStart,
        statement.CloseStatementDone,
        statement.BillMoney,
        statement.MoneyCount,
        module='sale_pos', type_='model')
    Pool.register(
        shop.ShopDailySummaryReport,
        sale.SaleReportSummary,
        sale.SaleReportSummaryByParty,
        sale.SaleDetailedReport,
        sale.SaleIncomeDailyReport,
        module='sale_pos', type_='report')
    Pool.register(
        sale.SaleDetailed,
        sale.SaleForceDraft,
        sale.SaleUpdateDate,
        shop.ShopDailySummary,
        sale.WizardAddProduct,
        sale.WizardSalePayment,
        sale.SaleIncomeDaily,
        sale.DeleteSalesDraft,
        statement.OpenStatement,
        statement.CloseStatement,
        module='sale_pos', type_='wizard')
