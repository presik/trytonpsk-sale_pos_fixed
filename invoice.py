# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval, Bool

__all__ = ['Invoice', ]


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    position = fields.Char('Position',)

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
        cls.party.states['readonly'] = Bool(Eval('number'))
