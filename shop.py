# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from datetime import time, datetime, timedelta

from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.report import Report


__all__ = [
    'SaleShop', 'ShopDailySummary', 'ShopDailySummaryReport',
    'ShopDailySummaryStart'
]

_ZERO = Decimal(0)


class SaleShop(metaclass=PoolMeta):
    __name__ = 'sale.shop'
    party = fields.Many2One('party.party', 'Default Party')
    invoice_sequence = fields.Many2One('ir.sequence.strict', 'Invoice Sequence')
    credit_note_sequence = fields.Many2One('ir.sequence.strict', 'Credit Note Sequence')
    self_pick_up = fields.Boolean('Default Self Pick Up',
        help='The goods are picked up by the customer before the sale, so no '
        'shipment is created.')
    pos_authorization = fields.Many2One('account.invoice.authorization', 'Pos Authorization', domain=[
        ('kind', '=', 'P')
    ])
    computer_authorization = fields.Many2One('account.invoice.authorization', 'Computer Authorization', domain=[
        ('kind', '=', 'C')
    ])
    electronic_authorization = fields.Many2One('account.invoice.authorization', 'Electronic Authorization', domain=[
        ('kind', 'in', ['1', '2', '3'])
    ])
    credit_note_electronic_authorization = fields.Many2One('account.invoice.authorization', 'Credit Note Electronic Authorization', domain=[
        ('kind', '=', '91')
    ])
    debit_note_electronic_authorization = fields.Many2One('account.invoice.authorization', 'Debit Note Electronic Authorization', domain=[
        ('kind', '=', '92')
    ])
    manual_authorization = fields.Many2One('account.invoice.authorization', 'Manual Authorization', domain=[
        ('kind', '=', 'M')
    ])
    freight_product = fields.Many2One('product.product', 'Freight Product')


class ShopDailySummaryStart(ModelView):
    'Shop Daily Summary Start'
    __name__ = 'sale_pos.shop_daily_summary.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    sale_date = fields.Date('Sale Date', required=True)
    shop = fields.Many2One('sale.shop', 'Shop',  required=True)
    early_morning_included = fields.Boolean('Early Morning Included')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_sale_date():
        Date = Pool().get('ir.date')
        return Date.today()


class ShopDailySummary(Wizard):
    'Shop Daily Summary'
    __name__ = 'sale_pos.shop_daily_summary'
    start = StateView('sale_pos.shop_daily_summary.start',
        'sale_pos.shop_daily_summary_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('sale_pos.shop_daily_summary_report')

    def do_print_(self, action):
        report_context = {
            'company': self.start.company.id,
            'sale_date': self.start.sale_date,
            'shop': self.start.shop.id,
            'early_morning_included': self.start.early_morning_included,
        }
        return action, report_context

    def transition_print_(self):
        return 'end'


class ShopDailySummaryReport(Report):
    'Shop Daily Summary'
    __name__ = 'sale_pos.shop_daily_summary_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ShopDailySummaryReport, cls).get_context(records, data)
        pool = Pool()
        Sale = pool.get('sale.sale')
        Company = pool.get('company.company')
        company = Company(data['company'])
        Shop = pool.get('sale.shop')
        Tax = pool.get('account.tax')
        Device = pool.get('sale.device')
        fixed_hour = time(6, 0)

        dom_sales = [
            ('shop', '=', data['shop']),
            ('company', '=', data['company']),
            ('number', '!=', None),
        ]
        if not data['early_morning_included']:
            dom_sales.append(('sale_date', '=', data['sale_date']))
        else:
            # Select sales including early morning of next day
            _start_date = datetime.combine(data['sale_date'], fixed_hour)
            _start_date = Company.convert_timezone(_start_date, True)

            end_date = data['sale_date'] + timedelta(days=1)
            _end_date = datetime.combine(end_date, fixed_hour)
            _end_date = Company.convert_timezone(_end_date, True)

            dom_sales.append(('sale_date', '>=', data['sale_date']))
            dom_sales.append(('sale_date', '<=', end_date))
            dom_sales.append(('create_date', '>=', _start_date))
            dom_sales.append(('create_date', '<=', _end_date))

        states_sale = ['processing', 'done']
        dom_sales.append(('state', 'in', states_sale))

        sales = Sale.search(dom_sales, order=[('number', 'ASC')])

        untaxed_amount = []
        tax_amount = []
        total_amount = []

        devices_ = Device.search([
            ('shop', '=', data['shop'])
        ])
        devices = {}
        for d in devices_:
            devices[d.id] = {
                'name': d.name,
                'code': d.code,
                'count_invoices': 0,
                'untaxed_amount': [],
                'tax_amount': [],
                'total_amount': [],
                'cash': [],
                'credit': [],
                'electronic': [],
                'other': [],
            }

        payment_modes = {
            'credit': [],
            'cash': [],
            'electronic': [],
            'other': [],
        }
        numbers = []
        categories = {}

        for sale in sales:
            if sale.total_amount <= 0:
                continue
            for invoice in sale.invoices:
                if not invoice.number or invoice.total_amount <= 0 or not sale.sale_device:
                    continue
                numbers.append(invoice.number)
                devices[sale.sale_device.id]['count_invoices'] += 1
                devices[sale.sale_device.id]['untaxed_amount'].append(invoice.untaxed_amount)
                devices[sale.sale_device.id]['tax_amount'].append(invoice.tax_amount)
                devices[sale.sale_device.id]['total_amount'].append(invoice.total_amount)
            untaxed_amount.append(invoice.untaxed_amount)
            tax_amount.append(invoice.tax_amount)
            total_amount.append(invoice.total_amount)
            if sale.payments:
                for payment in sale.payments:
                    if payment.statement.journal.kind and \
                        payment.statement.journal.kind in ['cash', 'credit', 'electronic']:
                        kind = payment.statement.journal.kind
                    else:
                        kind = 'other'
                    devices[sale.sale_device.id][kind].append(payment.amount)
                    payment_modes[kind].append(payment.amount)
            else:
                devices[sale.sale_device.id]['credit'].append(invoice.total_amount)
                payment_modes['credit'].append(invoice.total_amount)

            for line in invoice.lines:
                category_id = '0'
                if line.product.account_category:
                    category = line.product.account_category
                    if category.id not in list(categories.keys()):
                        categories[category.id] = {
                            'name': category.name,
                            'base': [line.amount],
                            'default_taxes': category.customer_taxes,
                            'taxes': {},
                        }
                        continue
                    category_id = category.id
                categories[category_id]['base'].append(line.amount)

        for k, v in list(categories.items()):
            base = sum(v['base'])
            categories[k]['base'] = base
            default_taxes = categories[k]['default_taxes']
            if default_taxes:
                for t in categories[k]['default_taxes']:
                    taxes = Tax.compute([t], base, 1)

                    if taxes:
                        tax = taxes[0]
                        categories[k]['taxes'][t.id] = {
                            'name': t.name,
                            'base': tax['base'],
                            'amount': tax['amount']
                        }
                    else:
                        categories[k]['taxes'][t.id] = {
                            'name': t.name,
                            'base': base,
                            'amount': _ZERO
                        }
            else:
                categories[k]['taxes'][0] = {
                    'name': 'EXCLUIDOS / EXENTOS',
                    'base': base,
                    'amount': _ZERO
                }

        if numbers:
            min_number = min(numbers)
            max_number = max(numbers)

        else:
            min_number = ''
            max_number = ''

        report_context['date'] = data['sale_date']
        report_context['company'] = company.party
        report_context['shop'] = Shop(data['shop']).name
        report_context['start_number'] = min_number
        report_context['end_number'] = max_number
        report_context['records'] = list(devices.values())
        report_context['categories'] = list(categories.values())
        report_context['sum_count_invoices'] = len(numbers)
        report_context['sum_untaxed_amount'] = sum(untaxed_amount)
        report_context['sum_tax_amount'] = sum(tax_amount)
        report_context['sum_total_amount'] = sum(total_amount)
        report_context['sum_cash'] = sum(payment_modes['cash'])
        report_context['sum_credit'] = sum(payment_modes['credit'])
        report_context['sum_electronic'] = sum(payment_modes['electronic'])
        report_context['sum_other'] = sum(payment_modes['other'])
        return report_context
